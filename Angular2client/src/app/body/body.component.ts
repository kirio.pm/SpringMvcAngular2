import { Component, OnInit, Output } from '@angular/core';
import {Http, Response}from '@angular/http';
import { EventEmitter } from "@angular/forms/src/facade/async";

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {
  @Output()
  changeTask: EventEmitter<number> = new EventEmitter();
  
	users:Array<any>;
  modeloItem: any;
  showAtt: Boolean;
  constructor(private http:Http) { }

  ngOnInit() {
    this.modeloItem={};
    this.getAlluser();
    
  }
  getAlluser():void{
  	this.http.request('http://localhost:8080/Spring4MVCAngularJSExample/user/')
  	.subscribe((resp:Response)=>{
  		this.users = resp.json();
  	});
  }

  saveUser():void{

    var user = { username: this.modeloItem.nuevonombre,
                 address: this.modeloItem.nuevadireccion,
                 email:  this.modeloItem.nuevoemail};
                 
    this.http.post('http://localhost:8080/Spring4MVCAngularJSExample/user/',user)
    .subscribe((resp:Response)=>{
      this.getAlluser();
    });


  }

delete(id):void{
  
this.http.delete('http://localhost:8080/Spring4MVCAngularJSExample/user/' + id)
.subscribe((resp:Response)=>{
  this.getAlluser();

    });

}
  activeEdition(id,name,address,email):void{
this.showAtt= true;
this.modeloItem.id=id;
this.modeloItem.nuevonombre=name;
this.modeloItem.nuevadireccion=address;
this.modeloItem.nuevoemail=email;
  }
  update(id,name,address,email):void{
    var user ={ 
                username: name,
                 address: address,
                 email:  email };

    this.http.put('http://localhost:8080/Spring4MVCAngularJSExample/user/' + id, user).subscribe((resp:Response)=>{
      this.changeTask.emit();
    });


  }


}
