import { EjmploPage } from './app.po';

describe('ejmplo App', function() {
  let page: EjmploPage;

  beforeEach(() => {
    page = new EjmploPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
